// MongoDB: Aggregation
/*
- MongoDB Aggregation is used to generate manipulated data and perform operations to create filtered results that helps in analyzing data.
- Compared to doing CRUD operations, aggreation gives us access to manipulate, filter, and compute for results. Providing us with information to make necessary development without having to create a frontend application.
*/

// Using the aggregated method
/*
- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage / aggregation process.
- Syntax:
	- { $match: { field : value } }
	- { $group: { _id: "value", fieldResult: "valueResult" } }

- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group all stocks for all suppliers found.

	- db.collectionName.aggregate([
		{$match: {fieldA: valueA}},
		{$group: {_id: "$fieldB"}, {result: {operation}}}
	])

- The "$" symbol will refer to a field name that is available in the documents that are being aggregate on.
- The "$sum" operator will total the value of all "stock" field in the returned documents that are found using the "$match" criteria.
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);


// Field projection with aggregation
/*
- The "$project" can be used when aggregating data to include / exclude fields from the returned results.
- Syntax:
	- { $project: { field 1 / 0 }}
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
])

// Sorting aggregated results
/*
- The "$sort" can be used to change the order of aggregated results.
- Providing a value of -1 will sort the aggregated results ina reverse order.
- Syntax:
	- {$sort: {field: 1 / -1}}
*/
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}}
])

// Aggregate results based on array fields
/*
- The "$unwind" deconstructs an array field from a collection / field with an array value to output a result of each element
- The syntax below will return results, creating separate documents for each of the countries provided per the "origin" field
- Syntax:
	- {$unwind: "$field"}
*/
db.fruits.aggregate([
	{$unwind: "$origin"}
])

// Display fruits documents by their origon and the kind of fruits that are supplied
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kinds: {$sum: 1}}}
])